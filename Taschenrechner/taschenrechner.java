import java.util.Scanner;

public class taschenrechner {

    public static void main(String[] args) throws InterruptedException {

        Scanner eingabe = new Scanner(System.in);


        System.out.println("-------------------------------------------------------------------------");
        System.out.println("|                                                                       |");
        System.out.println("|                          Taschenrechner V1.2                          |");
        System.out.println("|                                                                       |");
        System.out.println("-------------------------------------------------------------------------");

        Thread.sleep(1500);
        System.out.println("Guten Tag mit diesen Taschenrechner können sie addieren, subtrahieren, dividieren und multiplizieren.");
        Thread.sleep(2500);
        System.out.println("Addition = + ");
        Thread.sleep(1000);
        System.out.println("Subtraktion = - ");
        Thread.sleep(1000);
        System.out.println("Multiplikation = * ");
        Thread.sleep(1000);
        System.out.println("Division = / ");
        Thread.sleep(2000);

        System.out.print("Geben sie Ihre Rechenoperation ein. ");
        char zahl = eingabe.next().charAt(0);
    
        if (zahl == '+'){
            int zahl1c ; 
            int zahl2x ; 
            int erg1   ;
            System.out.println("Geben Sie Ihre erste Zahl ein"); 
            zahl1c=StdIn.intInput();
            System.out.println("Geben Sie Ihre zweit Zahl ein");
            zahl2x=StdIn.intInput();
            erg1=zahl1c + zahl2x;
            System.out.println("Das Ergebnis lautet: "+erg1);
            Thread.sleep(1000);
            System.out.println("Vielen Dank das Sie diesen Taschenrechner verwendet habe.");
            return;
        }

        if (zahl == '-'){
            int zahl1w ; 
            int zahl2q ; 
            int erg2   ;
            System.out.println("Geben Sie Ihre erste Zahl ein"); 
            zahl1w=StdIn.intInput();
            System.out.println("Geben Sie Ihre zweit Zahl ein");
            zahl2q=StdIn.intInput();
            erg2=zahl1w - zahl2q;
            System.out.println("Das Ergebnis lautet: "+erg2);
            Thread.sleep(1000);
            System.out.println("Vielen Dank das Sie diesen Taschenrechner verwendet habe.");
            return;
        }

        if (zahl == '*'){
            int zahl1e ; 
            int zahl2r ; 
            int erg3   ;
            System.out.println("Geben Sie Ihre erste Zahl ein"); 
            zahl1e=StdIn.intInput();
            System.out.println("Geben Sie Ihre zweit Zahl ein");
            zahl2r=StdIn.intInput();
            erg3=zahl1e * zahl2r;
            System.out.println("Das Ergebnis lautet: "+erg3);
            Thread.sleep(1000);
            System.out.println("Vielen Dank das Sie diesen Taschenrechner verwendet habe.");
            return;
        }

        if (zahl == '/'){
            int zahl1t ; 
            int zahl2z ; 
            int erg4   ;
            System.out.println("Geben Sie Ihre erste Zahl ein"); 
            zahl1t=StdIn.intInput();
            System.out.println("Geben Sie Ihre zweit Zahl ein");
            zahl2z=StdIn.intInput();
            erg4=zahl1t / zahl2z;
            System.out.println("Das Ergebnis lautet: "+erg4);
            Thread.sleep(1000);
            System.out.println("Vielen Dank das Sie diesen Taschenrechner verwendet habe.");
            return;
        }
        else {
        System.out.println("Bitte starten Sie das Programm neu.");
        }
    }
}

class StdIn 
{ 
  public static int intInput() 
  { 
    try{ 
      java.io.BufferedReader br = new java.io.BufferedReader (new java.io.InputStreamReader (System.in)); 
      return java.lang.Integer.parseInt (br.readLine()); 
    }catch (java.io.IOException ioe){ 
      return 0; 
    } 
  } 
}