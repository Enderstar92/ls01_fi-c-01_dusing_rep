import java.util.Scanner;

public class RomZahlen {

    public static void main(String[] args) {

    Scanner eingabe = new Scanner(System.in);

    System.out.print("Geben Sie Ihre römische Zahl ein. ");
    char rom = eingabe.next().charAt(0);

    if (rom == 'I'){
    System.out.println("Das ist die Zahl 1. ");
    }
    else if (rom == 'V'){
    System.out.println("Das ist die Zahl 10. ");
    }
    else if (rom == 'X'){
    System.out.println("Das ist die Zahl 50. ");
    }
    else if (rom == 'L'){
    System.out.println("Das ist die Zahl 100. ");
    }
    else if (rom == 'C'){
    System.out.println("Das ist die Zahl 500. ");
    }
    else if (rom == 'M'){
    System.out.println("Das ist die Zahl 1000. ");
    }
    else {
    System.out.println("ungültig");
    }
}
}