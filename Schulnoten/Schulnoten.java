import java.util.Scanner;

public class Schulnoten {

    public static void main(String[] args) {
        // TODO Auto-generadet method stub


        Scanner eingabe = new Scanner(System.in);

        System.out.print("Geben Sie Ihre Note ein: ");
        int note = eingabe.nextInt();

        switch (note) {

        case 1:
        case 2:
        case 3:
            System.out.println("Sehr Gut bis Befriedigend.");
            break;
        case 4:
            System.out.println("Ausreichend.");
            break;
        case 5:
            System.out.println("Mangelhaft.");
            break;
        case 6:
            System.out.println("Ungenügend.");
            break;
        default:
            System.out.println("Falsche Eingabe.");

        }
    }}